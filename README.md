# OKCast Archive
This is the archive for the OKCast, a podcast that ran from November 2013 to June 2015. The archive contains the publicly published, edited interviews that were released to the podcast feed and the okcast.org web site. The unedited interviews are unavailable because of an agreement between interviewees and the interviewer that interviews would be edited. Private and specific requests for audio files will be considered on an individual basis. 

The podcast was described as:
The OKCast is a weekly open source blog and podcast with the goal to explore, connect, use and inspire open knowledge projects around the world to develop the public commons, improve organization and government transparency and communication, and advocate for social justice and social activism.

